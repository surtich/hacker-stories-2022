import { useEffect, useRef, useState } from "react";
import { useOutletContext } from "react-router-dom";
import { getStory } from "./services";
import List from "./List";

export default function Favorites() {
    const { favorites } = useOutletContext();
    const [stories, setStories] = useState(favorites.map(favorite => ({ objectID: favorite.objectID, isLoading: true })));
    const favoritesLoaded = useRef(0);


    useEffect(() => {
        if (favoritesLoaded.current < favorites.length) {
            favorites.forEach(async (favorite, i) => {
                const story = await getStory(favorite.objectID);
                setStories(stories => {
                    const newStories = [];
                    for (let j = 0; j < stories.length; j++) {
                        newStories[j] = stories[j];
                    }
                    favoritesLoaded.current = favoritesLoaded.current + 1;
                    newStories[i] = { ...story, isFavorite: true, favoriteId: favorite.id };
                    return newStories;
                });
            });
        } else {
            setStories(stories => stories.filter(story => favorites.find(favorite => favorite.id === story.favoriteId)));
        }



    }, [favorites]);

    return (<div>
        FAVORITES
        <br />
        {
            <List list={stories} />
        }

    </div>)
} 