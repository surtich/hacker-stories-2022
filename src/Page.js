import styled, { css } from 'styled-components';

const ButtonPage = styled.button`
    font-size: 20px;
    ${props => props.selected && css`
        background-color: green;
        color: white; 
    `}
`;

const Page = function ({ numPage, selected, selectPage }) {
    function handleClick(e) {
        selectPage(numPage)
    }
    return <ButtonPage selected={selected} onClick={handleClick}>{numPage}</ButtonPage>
};

export default Page;