import { useEffect, useState, useRef, useCallback } from "react";
import List from "./List";
import Search from "./Search";
import useSemiPersistentState from "./useSemiPersistentState";
import Pagination from "./Pagination";
import { useNavigation, useOutletContext } from "react-router-dom";
import { getStories } from "./services";


function App() {

  const [searchTerm, setSearchTerm] = useSemiPersistentState('search', 'react');
  const [stories, setStories] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [actualPage, setActualPage] = useState(0);
  const [totalPages, setTotalPages] = useState(0);
  const [version, setVersion] = useState(1);
  const versionRef = useRef(0);
  const navigation = useNavigation();
  const { favorites } = useOutletContext();
  const [storiesLoaded, setStoriesLoaded] = useState(false);


  useEffect(() => {
    if (!storiesLoaded) {
      versionRef.current = versionRef.current + 1;
      setVersion(version => {
        setIsLoading(true);
        getStories(searchTerm, actualPage).then(result => {
          if (version === versionRef.current) {
            setStoriesLoaded(true);
            setStories(_ => result.results.map(story => {
              const favorite = favorites.find(favorite => favorite.objectID === story.objectID);
              const favoriteId = favorite ? favorite.id : null;
              return { ...story, isFavorite: !!favorite, favoriteId }
            }));
            setTotalPages(result.totalPages);
            setIsLoading(false);
          }
        });
        return version + 1

      });
    } else {
      setStories(stories => stories.map(story => {
        const favorite = favorites.find(favorite => favorite.objectID === story.objectID);
        const favoriteId = favorite ? favorite.id : null;
        return { ...story, isFavorite: !!favorite, favoriteId }
      }));
    }
  }, [searchTerm, actualPage, favorites, storiesLoaded]);

  const changeSearchTerm = useCallback((term) => {
    setSearchTerm(term);
    setActualPage(0);
    setStoriesLoaded(false);
  }, [setSearchTerm]);

  function changeActualPage(newPage) {
    setStoriesLoaded(false);
    setActualPage(newPage);
  }


  if (navigation.state === "loading") {
    return <div>loading...</div>
  }


  return (
    <div>
      <Search searching={isLoading} searchTerm={searchTerm} setSearchTerm={changeSearchTerm} />
      <hr />
      {isLoading ? <p>Loading...</p> :
        <>
          <List list={stories} />
          <Pagination totalPages={totalPages} actualPage={actualPage} setActualPage={changeActualPage} />
        </>
      }
    </div>
  );
}
export default App;


