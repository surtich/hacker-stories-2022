import React from 'react';
import ReactDOM from 'react-dom/client';
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import './index.css';
import App from './App';
import Favorites from './Favorites';
import { getFavorites } from './services';
import Root from './Root';

const USERID = 23;

const router = createBrowserRouter([
  {
    path: "/",
    loader: () => getFavorites(USERID),
    element: <Root />,
    children: [
      {
        path: "/home",
        element: <App />,
      },
      {
        path: "/favorites",
        element: <Favorites />,
      },
    ]
  }
]);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <RouterProvider router={router} />
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
//reportWebVitals();
