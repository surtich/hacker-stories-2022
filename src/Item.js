import { useOutletContext } from 'react-router-dom';
import './Item.scss';

function Item({ item }) {

    const { removeFavorite, addFavorite } = useOutletContext();

    function handleRemoveFavorite(story) {
        removeFavorite(story.favoriteId)
    }

    function handleAddFavorite(story) {
        addFavorite(story.objectID)
    }

    if (item.isLoading) {
        return <div>loading...</div>
    }
    return <li>
        <span>
            <a href={item.url}>{item.title}</a>
        </span>
        <span className="item-author">{item.author}</span>
        <span>{item.num_comments}</span>-
        <span>{item.points}</span>
        {item.isFavorite ? <button onClick={() => handleRemoveFavorite(item)}>Remove favorite</button> : <button onClick={() => handleAddFavorite(item)}>Add favorite</button>}
    </li>
}

export default Item;