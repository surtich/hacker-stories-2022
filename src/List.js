import Item from './Item';

function List({ list }) {

    return (
        <ul>
            {list.map(function (item) {
                return (
                    <Item item={item} key={item.objectID} />
                );
            })}
        </ul>
    );
}


export default List;
