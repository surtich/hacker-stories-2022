import { useEffect, useState } from "react";

function Search({ searchTerm, setSearchTerm, searching }) {


    const [value, setValue] = useState(searchTerm);

    useEffect(() => {
        const timeoutId = setTimeout(() => {
            setSearchTerm(value);
        }, 200);
        return function () {
            clearTimeout(timeoutId);
        }
    }, [value, setSearchTerm]);

    const handleChange = (e) => {
        setValue(e.target.value);
        e.preventDefault();
    };

    return (
        <>
            <label htmlFor="search">Search: </label>
            <input id="search" type="text" defaultValue={value} onChange={handleChange} />
            {searching && <p>
                Searching for <strong>{searchTerm}</strong>.
            </p>}

        </>
    );
}


export default Search;
