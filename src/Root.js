import { useState } from "react";
import { Link, Outlet, useLoaderData } from "react-router-dom";
import { removeFavorite as serviceRemoveFavorite, addFavorite as serviceAddFavorite } from './services';

export default function Root() {
    const [favorites, setFavorites] = useState(useLoaderData());

    function removeFavorite(favoriteId) {
        serviceRemoveFavorite(favoriteId).then(_ => setFavorites(favorites.filter(favorite => favorite.id !== favoriteId)));
    }

    function addFavorite(objectID) {
        serviceAddFavorite(objectID).then(favorite => setFavorites([...favorites, favorite]));
    }

    return (
        <>
            {/* all the other elements */}
            <div id="detail">
                <Link to={`/home`}>home</Link>
                <br />
                <Link to={`/favorites`}>favorites</Link>
                <Outlet context={{ favorites, removeFavorite, addFavorite }} />
            </div>
        </>
    );
}